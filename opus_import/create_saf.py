#!/usr/bin/env python3

"""Simple skript to create csv from OPUS."""

import sys
import argparse
import time
import csv
import re
import string
import os.path
import unicodedata
from datetime import datetime
import pymysql
import tzlocal  # $ pip install tzlocal
import ftfy
import ftfy.bad_codecs

import parse_source_title
from util import print_quiet, print_verbose

SQL_DB_ADDRESS = 'opac.server.tld'
DB_NAME = 'dbname'
USER = 'username'
PASS = 'password'

FILENAME_HEADER = 'files'

OUFILE = 'professorship.csv'

def main():
    """main"""
    args = parse_arguments()
    items = load_items(args)
    write_csv(items, args)
    print_quiet("Done", args)

def parse_arguments():
    """parse cli arguments"""
    parser = argparse.ArgumentParser(description="""Simple skript to create csv from OPUS.

Example: ./create_saf.py" -s -a "default" -d dummy.pdf 255030""")
    parser.add_argument('ounumber', metavar='ounumber', type=int,
                        help='kst, for which the csv should be created')
    parser.add_argument('-o', '--output', metavar='filename', default='out.csv', dest='out_file',
                        help='output filename, defaults to out.csv')
    parser.add_argument('-d', '--dummy-file', metavar='filename', dest='dummy_file', help="""
filename of the dummy file. If not specified, the column won\'t be created""")
    parser.add_argument('-x', '--disable-regex', help="don't use regex", action="store_true",
                        dest="no_regex")
    parser.add_argument('-a', '--anystyle', metavar='model', dest="anystyle",
                        help="""either "default" to use anystyle\'s default model, or the path to a
.mod file to use instead.""")
    parser.add_argument('-v', '--verbose', help="increase output verbosity", action="store_true",
                        dest="verbose")
    parser.add_argument('--patents', help="consider only patents", action="store_true",
                        dest="patents")
    parser.add_argument('-q', '--quiet', help="no console output", action="store_true",
                        dest="quiet")
    parser.add_argument('-i', '--interactive', action="store_true", dest="interactive",
                        help="interactively correct information extracted from source_title")
    parser.add_argument('-s', '--stats', action="store_true", dest="stats",
                        help="print stats of source_title extraction.")
    parser.add_argument('-p', '--prefer', dest="prefer", choices=["regex", "anystyle"],
                        help="""which extraction method should be preferred. The other option will
be used to fill in gaps. Defaults to "anystyle".""")
    parser.add_argument('-t', '--training', metavar='filename', dest='training', help="""create
anystyle training data and save it in the file. Should be used together with --interactive.
EXPERIMANTAL - some source_titles may cause problems, e.g. "xyz - 2019 S. 19- 20" will see the 19
from the 2019 as the startpage in the sequence.""")
    return parser.parse_args()

def combine(fields):
    """combines a list of multiple values into a single string representing a "||"-separated list"""
    while None in fields:
        fields.remove(None)
    return "||".join(fields)

def load_items(args):
    """loads all items in a dictionary"""
    print_verbose("Connecting to database...", args)
    database = pymysql.connect(host=SQL_DB_ADDRESS, user=USER, password=PASS, database=DB_NAME)
    if not database:
        print("Failed to connect to database!")
        sys.exit(1)
    print_verbose("Connected!", args)

    # find institute_de.nr
    cursor = database.cursor(pymysql.cursors.DictCursor)
    sql = ('SELECT institute_de.nr FROM institute_de WHERE ' +
           'institute_de.kst = ' + str(args.ounumber) + ';')
    if not cursor.execute(sql):
        print("kst " + str(args.ounumber) + " not found in institute_de, exiting.")
        database.close()
        sys.exit(1)
    data = cursor.fetchone()
    institute_nr = data["nr"]
    print_quiet("institute_de.nr found: " + institute_nr, args)

    # find source_opus list
    sql = ('SELECT opus_inst.source_opus FROM opus_inst WHERE '
           'opus_inst.inst_nr = ' + institute_nr + ';')

    if not cursor.execute(sql):
        print("No opus_source found in opus_inst for inst_nr " + institute_nr +", exiting.")
        database.close()
        return {}
        #sys.exit(1)
    data = cursor.fetchall()
    #data = (cursor.fetchone(),) # only for testing purposes, only checks only first entry
    source_opus_list = []
    for source_opus in data:
        source_opus_list.append(source_opus["source_opus"])
    #source_opus_list = [6249] # only for testing purposes, found in opus_diss
    opus_count = len(source_opus_list)

    # load all items
    print_quiet("Loading " + str(opus_count) + " opus_source", args)
    items = {}
    for count, item_id in enumerate(source_opus_list, 1):
        print_verbose("Loading item: " + str(item_id) + " (" + str(count) + "/" +
                      str(opus_count) + ")", args)
        sql = ('SELECT * FROM opus WHERE opus.source_opus = ' + str(item_id) + ';')
        cursor.execute(sql)
        data = cursor.fetchone()
        if (data["type"] == 60 and not args.patents) or (data["type"] != 60 and args.patents):
            continue
        items[item_id] = data.copy()
        items[item_id]["opus_id"] = item_id
        items[item_id]["ou_nr"] = args.ounumber
        items[item_id]["institute_nr"] = institute_nr

        # checking opus_autor
        sql = ('SELECT * FROM opus_autor WHERE opus_autor.source_opus = ' + str(item_id) + ';')
        if cursor.execute(sql):
            data = cursor.fetchall()
            # sort authors by order
            data = sorted(data, key=lambda k: k['reihenfolge'])
            for key in data[0]:
                items[item_id][key] = []
            items[item_id]["raw_ou"] = []
            for autor in data:
                for key, item in autor.items():
                    if key == "creator_kst":
                        items[item_id]["raw_ou"].append(item)
                        item = prettify_affiliation(item)
                    items[item_id][key].append(item)
            for key in data[0]:
                while None in items[item_id][key]:
                    items[item_id][key].remove(None)
                while "" in items[item_id][key]:
                    items[item_id][key].remove("")
                for index, value in enumerate(items[item_id][key]):
                    if not isinstance(value, str):
                        items[item_id][key][index] = str(value)
                items[item_id][key] = combine(items[item_id][key])
            items[item_id]["raw_ou"] = combine(items[item_id]["raw_ou"])
        else:
            print("opus_autor for source_opus " + str(item_id) + " not found")

        # checking opus_diss
        sql = ('SELECT * FROM opus_diss WHERE opus_diss.source_opus = ' + str(item_id) + ';')
        if cursor.execute(sql):
            print_verbose("Found item in opus_diss", args)
            data = cursor.fetchone()
            for key, item in data.items():
                items[item_id][key] = item

    print_quiet("Finished loading items.", args)

    database.close()
    print_verbose("Connection closed!", args)

    return items

def write_csv(items, args):
    """Write item information to csv"""

    print_quiet("Creating CSV file " + args.out_file, args)

    # order of the rows in the table
    order = ["dc.title", "dc.contributor.author", "dc.contributor.affiliation", "dc.type",
             "dc.language.iso", "dc.description.abstract", "dc.subject", "dcterms.extent",
             "dc.identifier.doi", "dc.identifier.isbn", "dc.identifier.url", "dc.identifier.uri",
             "dc.contributor.advisor", "dc.date.submitted", "dc.date.issued", "dc.date.available",
             "dcterms.accessRights", "dc.publisher", "dc.relation.isbn",
             "dc.relation.doi", "dc.relation.ispartof", "dc.relation.issn",
             "dc.description.volume", "dc.description.issue", "dc.description.startpage",
             "dc.description.endpage", "dc.description.sponsorship", "payment.unit",
             "payment.payer", "payment.netAmount", "dc.description", "dc.description.intern",
             "tucbib.opus.id", "tucbib.opus.sourcetitle", "tucbib.ou.nr", "tucbib.institute.nr",
             "tucbib.publication.peerreviewed", "collections"]
    #TODO add "dc.relation.publication", seems to be considered as entity which has to exist already

    # list the source_title in a seperate list and the keys in anouter one. Skip empty source_title
    extract_source_title(items, args)

    item_count = len(items)
    with open(args.out_file, 'w+') as output:
        writer = csv.writer(output)
        header = order
        if args.dummy_file:
            header = [FILENAME_HEADER] + order
        writer.writerow(header)

        for count, item in enumerate(items.values()):
            print_verbose("writing item " + str(count+1) + "/" + str(item_count), args)
            writer.writerow(create_line(map_vocab(item, args), order, args.dummy_file))

def multiple_values(s):
    if s:
        s = s.replace(",", "||").replace(";", "||")
    return s

def map_vocab(item, args):
    """maps the OPUS column names (in item) to the dc vocabulary (in temp)"""
    temp = {}
    # set preferred language to match the description language
    preferred_lang = item.get("description_lang", '')
    if not item.get("description", '') and item.get("description2", ''):
        # if description is empty, but description2 has content, then switch the language to eng
        preferred_lang = "eng"
    if preferred_lang == "ger":
        # german
        temp["dc.title"] = try_options(item, ["title_de", "title", "title_en"])
        temp["dc.description.abstract"] = try_options(item, ["description", "description2"])
        temp["dc.subject"] = try_options(item, ["subject_uncontrolled_german",
                                                "subject_uncontrolled_english"])
    elif preferred_lang == "eng":
        # english
        temp["dc.title"] = try_options(item, ["title_en", "title", "title_de"])
        temp["dc.description.abstract"] = try_options(item, ["description2", "description"])
        temp["dc.subject"] = try_options(item, ["subject_uncontrolled_english",
                                                "subject_uncontrolled_german"])
    else:
        # unkown or another language, prefers english
        temp["dc.title"] = try_options(item, ["title", "title_en", "title_de"])
        temp["dc.description.abstract"] = try_options(item, ["description", "description2"])
        temp["dc.subject"] = try_options(item, ["subject_uncontrolled_english",
                                                "subject_uncontrolled_german"])

    ligatures = {0x0C: u'fi'}
    temp["dc.description.abstract"] = temp["dc.description.abstract"].translate(ligatures)

    #temp["dc.description.abstract"] = unicodedata.normalize("NFKD",temp["dc.description.abstract"])

    temp["dc.subject"] = '||'.join(x.strip() for x in temp["dc.subject"].split(','))
    #temp["dc.subject"] = re.sub(r"\s+", "", temp["dc.subject"], flags=re.UNICODE)

    languages = {
        "eng":  "en",
        "ger":  "de"
    }

    temp["dc.language.iso"] = languages.get(preferred_lang, "other")

    temp["dc.contributor.author"] = item.get("creator_name", '')
    temp["dc.contributor.affiliation"] = item.get("creator_kst", '')

    types = {
        8:  "doctoral thesis",
        24: "thesis",
        50: "periodical",
        51: "article",
        52: "article",
        53: "book part",
        54: "book",
        55: "book",
        56: "patent",
        58: "other type of report",
        59: "other type of report",
        60: "conference paper",
        61: "conference paper",
        62: "conference object",
        63: "conference poster",
        64: "preprint"
    }

    peerreviewed = {
        8:  "0",
        24: "0",
        50: "1",
        51: "1",
        52: "0",
        53: "1",
        54: "2",
        55: "0",
        56: "0",
        58: "0",
        59: "0",
        60: "1",
        61: "0",
        62: "0",
        63: "0",
        64: "0"
    }

    temp["dc.type"] = types.get(int(item.get("type", '0')), "other")
    temp["tucbib.publication.peerreviewed"] = peerreviewed.get(int(item.get("type", '0')), "other")

    temp["dcterms.extent"] = item.get("umfang", '')
    temp["dc.identifier.doi"] = multiple_values(item.get("doi", ''))
    temp["dc.identifier.isbn"] = multiple_values(item.get("isbn", ''))
    temp["dc.identifier.url"] = multiple_values(item.get("url", ''))
    temp["dc.identifier.uri"] = multiple_values(item.get("doi_urn", ''))

    if temp["dc.identifier.uri"]:
        doisearch = re.search("10\.[a-zA-z0-9\.\/\-]*", temp["dc.identifier.uri"])
        if doisearch:
            uri_is_doi = doisearch.group(0)
            if uri_is_doi and not temp["dc.identifier.doi"]:
                temp["dc.identifier.doi"] = uri_is_doi

    temp["dc.contributor.advisor"] = item.get("advisor", '')
    temp["dc.date.submitted"] = item.get("date_accepted", '')
    temp["dc.date.issued"] = item.get("date_year", '')

    if item.get("date_creation", ''):
        unix_timestamp = float(item.get("date_creation", ''))
        local_timezone = tzlocal.get_localzone() # get pytz timezone
        local_time = datetime.fromtimestamp(unix_timestamp, local_timezone)

        creation_date = local_time.strftime("%d.%m.%Y")
        temp["dc.date.available"] = creation_date

    rights = {
        "j":    "http://purl.org/eprint/accessRights/OpenAccess",
        "y":    "http://purl.org/eprint/accessRights/OpenAccess",
        "n":    "n"
    }

    temp["dcterms.accessRights"] = rights.get(item.get("oa_publ", ''), "")
    temp["dc.publisher"] = item.get("publisher", '')
    temp["dc.description.sponsorship"] = item.get("foerderung", '')
    temp["payment.unit"] = item.get("mittelherkunft", '')
    temp["payment.payer"] = item.get("creator_kst", '')
    temp["payment.netAmount"] = item.get("publ_kosten", '')
    temp["dc.description"] = item.get("bem_extern", '')
    temp["dc.description.intern"] = item.get("bem_intern", '')

    temp["dc.relation.publication"] = item.get("publication", '')
    temp["dc.relation.isbn"] = multiple_values(item.get("isbn2", ''))
    temp["dc.relation.doi"] = multiple_values(item.get("doi2", ''))
    temp["dc.relation.ispartof"] = item.get("ispartof", '')
    temp["dc.relation.issn"] = multiple_values(item.get("issn", ''))
    temp["dc.description.volume"] = item.get("volume", '')
    temp["dc.description.issue"] = item.get("issue", '')
    temp["dc.description.startpage"] = item.get("start_page", '')
    temp["dc.description.endpage"] = item.get("end_page", '')

    # TODO: dc.relation.conference
    # TODO: dc.relation.grantno
    # TODO: dc.relation (for project)

    temp["tucbib.opus.id"] = item.get("opus_id", '')
    temp["tucbib.opus.sourcetitle"] = item.get("source_title", '')
    temp["tucbib.ou.nr"] = item.get("ou_nr", '')
    temp["tucbib.institute.nr"] = item.get("institute_nr", '')

    #write owning_collection and other_collections to file
    collections = item.get("raw_ou").split("||")
    while args.ounumber in collections:
        collections.remove(args.ounumber)
    while "extern" in collections:
        collections.remove("extern")
    collections = list(dict.fromkeys(collections))
    collections.insert(0, args.ounumber)
    temp["collections"] = "||".join(collections)

    return temp

def try_options(item, options):
    """Return value of first key with truthfy value from a list of options."""
    for option in options:
        if item.get(option, ''):
            return item[option]
    return ''

def create_line(parts, order, dummy_file):
    """sorts the values of a dictionary in a list for the csv file"""
    line = []
    if dummy_file:
        line = [dummy_file]
    for value in order:
        content = parts.get(value, '')
        if content and isinstance(content, str):
            content = ftfy.fix_text(content, normalization='NFKC')
        line.append(content)
    return line

def extract_source_title(items, args):
    """extract information from source_title"""
    print_verbose("collection source_title", args)
    source_title_list = []
    key_list = []
    for key, values in items.items():
        source_title_list.append(values.get("source_title", ""))
        key_list.append(key)

    start_time = time.time()
    # actual extraction done in parse_source_title module
    parsed_list = parse_source_title.parse_list(source_title_list, args)
    if args.stats:
        print_stats(source_title_list, parsed_list, start_time)

    # put the extracted info in the corresponding dictionary
    for index, item in enumerate(parsed_list):
        for result_key, result_value in item.items():
            items[key_list[index]][result_key] = result_value

    if args.training:
        print_quiet("\nCreating xml training data", args)
        sequence_list = []
        for index, vals in enumerate(parsed_list):
            sequence = parse_source_title.create_sequence(source_title_list[index], vals)
            sequence_list.append(sequence)
        parse_source_title.write_xml(sequence_list, args.training)

def prettify_affiliation(ou):
    if os.path.isfile(OUFILE):
        kstlist = [line.rstrip('\n') for line in open(OUFILE)]
        for kst in kstlist:
            if kst.startswith(ou + ";"):
                kstinfo = kst.split(";")
                if len(kstinfo) > 3:
                    return ou + " - " + kstinfo[3]
    return ou

def print_stats(source_title_list, parsed_list, start_time):
    """prints how many of each possible substring where found in total, and the total time needed"""
    end_time = time.time()
    option_list = ["publication", "volume", "issue", "start_page", "end_page", "doi", "part-of"]
    result = [0]*7
    for vals in parsed_list:
        for index, key in enumerate(option_list):
            if str(vals.get(key, '')).strip():
                result[index] += 1
    for i, val in enumerate(result):
        result[i] = str(round(val/max(1, len(source_title_list))*100.0, 2))+"%"
    print("\nResults of source_title extraction:\nPublication:\t"+result[0])
    print("Volume:\t\t"+result[1])
    print("Issue:\t\t"+result[2])
    print("Startpage:\t"+result[3])
    print("Endpage:\t"+result[4])
    print("DOI:\t\t"+result[5])
    print("ispartof:\t"+result[6])
    print("Elapsed Time:\t"+str(round((end_time-start_time)*1000))+"ms\n")

if __name__ == "__main__":
    main()
