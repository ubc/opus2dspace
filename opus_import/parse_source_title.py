"""parses source_title from OPUS and tries to extract information."""

import re
#import urllib.request
#import urllib.parse
#import time
import json
import os
import subprocess
import readline
import xml.dom.minidom

import tempfile

from util import print_quiet, print_verbose

BAD_CHARS = " \n.,-–:"

def parse(source_title):
    """parses source_title
    doi2 → dc.relation.doi"""
    source_title = source_title.strip()
    new_info = {}

    if source_title[:3] == "In:":
        new_info = handle_format_2(source_title)
    elif " : " in source_title:
        new_info = handle_format_1(source_title)
    match = re.compile("(10[.][0-9]{4,}(?:[.][0-9]+)*/(?:(?![\"&\'])\\S)+)").search(source_title)
    if match:
        new_info["doi2"] = source_title[match.start():match.end()]
    if not new_info.get("year", 0):
        new_info = find_additional_year(new_info, source_title)
    if not new_info.get("start_page", 0):
        new_info = find_additional_pages(new_info, source_title)
    if not new_info.get("publication", ''):
        new_info = find_additional_publication(new_info, source_title)
    if not new_info.get("volume", 0):
        new_info = find_additional_volume(new_info, source_title)
    if not new_info.get("issue", 0):
        new_info = find_additional_issue(new_info, source_title)
    if new_info.get("start_page", 0) and not new_info.get("end_page", 0):
        new_info["end_page"] = new_info["start_page"]
    if new_info.get("end_page", 0) and not new_info.get("start_page", 0):
        new_info["start_page"] = new_info["end_page"]
    new_info["part-of"] = ''

    # code to access api.crossref.org, but it didn't deliver useful results
#    print("\n"+source_title)
#    print("Publication:\t"+new_info.get("publication", ''))
#    print("Volume:\t\t"+str(new_info.get("volume", '')))
#    print("Issue:\t\t"+new_info.get("issue", ''))
#    print("Startpage:\t"+str(new_info.get("start_page", '')))
#    print("Endpage:\t"+str(new_info.get("end_page", '')))

#    #Query api.crossref.org to find possible matches
#    query_dict = {"query.bibliographic":source_title}
#    query_url = "https://api.crossref.org/works?"+urllib.parse.urlencode(query_dict)
#    response = json.load(urllib.request.urlopen(query_url))
#    if response.get("status", '') != "ok":
#        print("Error communicating with api.crossref.org")
#        return (form, new_info)
#    for match_item in response["message"]["items"]:
#        unlikely = True
#        if unlikely:
#            continue
#        print("possible crossref match found")
#        query_url = "http://dx.doi.org/"+match_item["DOI"]
#        try:
#            with urllib.request.urlopen(query_url) as response2:
#                print("Possible DOI: http://dx.doi.org/"+match_item["DOI"])
#        except:
#            print("doi not found")
    return new_info

def find_additional_publication(new_info, source_title):
    """tries to find publication name"""
    match = re.compile(",\\s\\d").search(source_title)
    if match:
        # from the beginning to ", \d" so it ends before a number, e.g. "Publication, 2020"
        new_info["publication"] = source_title[:match.end()-3]
    else:
        match = re.compile(".+?,").match(source_title)
        if match:
            # from the beginning until the first "," e.g. "Publication, TU Chemnitz, S. 1-5"
            new_info["publication"] = source_title[:match.end()-1]
        else:
            match = re.compile("[\\w\\s\\.\\-\\/\\\"]+").match(source_title)
            if match:
                end = match.end()
                match = re.compile("[A-Za-z]{3,}").search(source_title[:end])
                if match:
                    # from beginning until non-title character is encountered, e.g. Publication : …
                    new_info["publication"] = source_title[:end]
    return new_info

def find_additional_year(new_info, source_title):
    """tries to find the year of the publication"""
    match = re.compile("(?<=\\D)\\d{4}(?=\\D)").search(source_title)
    if match:
        # exactly 4 digits in a row
        new_info["year"] = int(source_title[match.start():match.end()])
        return new_info
    match = re.compile("^\\d{4}(?=\\D)").search(source_title)
    if match:
        # ending with exactly 4 digits
        new_info["year"] = int(source_title[match.start():match.end()])
    return new_info

def find_additional_pages(new_info, source_title):
    """tries to find pages of the publication"""
    match = re.compile("((p[p]?)|([Ss]))[\\.]?[\\s]*[0-9]+[\\s]*-[\\s]*[0-9]+").search(source_title)
    if match:
        match = re.compile("[0-9]+[\\s]*-[\\s]*[0-9]+").search(source_title)
        if match:
            # S. or pp. or p. followed by [start_page] - [end_page]
            temp = source_title[match.start():match.end()].split("-")
            new_info["start_page"] = int(temp[0].strip())
            new_info["end_page"] = int(temp[1].strip())
        return new_info
    match = re.compile("((p[p]?)|([Ss]))[\\.]?[\\s]*[0-9]+").search(source_title)
    if match:
        # S. or pp. or p. followed by [page]
        pos = match.start()
        match = re.compile("[0-9]+").search(source_title[match.start():match.end()])
        new_info["start_page"] = int(source_title[pos+match.start():pos+match.end()])
    return new_info

def find_additional_volume(new_info, source_title):
    """tries to find volume of the publication"""
    match = re.compile("[Vv][Oo][Ll]\\.??\\s??[\\d]+").search(source_title)
    if match:
        # Vol. [volume]
        match2 = re.compile("[0-9]+").search(source_title[match.start():match.end()])
        temp = source_title[match.start():match.end()][match2.start():match2.end()]
        new_info["volume"] = int(temp.strip())
    else:
        match = re.compile("[Vv][Oo][Ll][Uu][Mm][Ee]\\s??[\\d]+").search(source_title)
        if match:
            # Volume [volume]
            match2 = re.compile("[0-9]+").search(source_title[match.start():match.end()])
            temp = source_title[match.start():match.end()][match2.start():match2.end()]
            new_info["volume"] = int(temp.strip())
        else:
            # [year] [volume] [pages]
            pattern = "(?<=\\D\\d{4}\\D)\\s*?\\d+?\\s*?(?=(S\\.|p?p\\.|,))"
            match = re.compile(pattern).search(source_title)
            if match:
                new_info["issue"] = source_title[match.start():match.end()].replace(",", "").strip()
    return new_info

def find_additional_issue(new_info, source_title):
    """tries to find issue of the publication"""
    match = re.compile("[Ii][Ss][Ss][Uu][Ee]:??\\.??\\s??\\d[\\d\\/-]*").search(source_title)
    if match:
        # issue [issue]
        temp = source_title[match.start():match.end()]
        new_info["issue"] = temp.strip()
    else:
        match = re.compile("Ausg(abe)??:??\\.??\\s??\\d[\\d\\/-]*").search(source_title)
        if match:
            #Ausgabe [issue]
            temp = source_title[match.start():match.end()]
            new_info["issue"] = temp.strip()
        else:
            match = re.compile("[\\dIVX-]+?[\\s\\.-]+?Aus(gabe)").search(source_title)
            if match:
                #[issue] Ausgabe
                temp = source_title[match.start():match.end()]
                new_info["issue"] = temp.strip()
            else:
                match = re.compile(".*\\.\\s-").search(source_title)
                if match:
                    pattern = "[\\s]*[\\d]+(?=\\.?\\s+\\d{4})"
                    match = re.compile(pattern).match(source_title[match.end():])
                    if match:
                        # [title], - [issue] [year]
                        new_info["volume"] = int(match.group())
    return new_info


def handle_format_1(source_title):
    """parse a format of:

    <publication> - <place> : <publisher> , <year> , S. <start_page> - <end_page> [Bd. <volume>]

    publication → dc.relation.publication
    start_page → dc.description.startpage
    end_page → dc.description.endpage
    volume → dc.description.volume
    issue → dc.description.issue
    """
    publication = ''
    volume = ''
    start_page = ''
    end_page = ''
    publisher = ''
    year = ''
    match = re.compile("\\s-(?!.*?\\s-[^:]*:).*?:").search(source_title)
    if match:
        publication = source_title[:match.start()].strip()
    match = re.compile("(Band|Bd)\\.?\\s*?\\d+").search(source_title)
    if match:
        match = re.compile("\\d+").findall(source_title[match.start():match.end()])
        volume = match[0]
    match = re.compile("\\b(S|p?p)\\.?\\s?(?=\\d+)").search(source_title)
    if match:
        pos = match.end()
        match = re.compile("^\\d+").search(source_title[match.end():])
        start_page = source_title[pos+match.start():pos+match.end()]
        pos = pos+match.end()
        match = re.compile("\\s?-\\s?(?=\\d+)").search(source_title[pos:])
        if match:
            pos = pos+match.end()
            match = re.compile("^\\d+").search(source_title[pos:])
            end_page = source_title[pos+match.start():pos+match.end()]
    else:
        match = re.compile("\\d+\\s?(?=(S|p?p))").search(source_title)
        if match:
            start_page = source_title[match.start():match.end()]
            end_page = start_page
    match = re.compile("(?<=:\\s)[^:]+?(?=,\\s?\\d{4})").search(source_title)
    if match:
        publisher = source_title[match.start():match.end()]
    match = re.compile("\\D\\d{4}(?=(\\D|,)\\s?(S|p?p))").search(source_title)
    if match:
        year = source_title[match.start()+1:match.end()]

    item = {}
    item["publication"] = publication
    item["volume"] = volume
    item["start_page"] = start_page
    item["end_page"] = end_page
    item["publisher"] = publisher
    item["year"] = year
    return item

def handle_format_2(source_title):
    """parse a format of:

    In: <publication>. [- <issue>.] <year> [, <volume>] [S. <start_page> - <end_page>]

    publication → dc.relation.publication
    start_page → dc.description.startpage
    end_page → dc.description.endpage
    volume → dc.description.volume
    issue → dc.description.issue
    """
    publication = ''
    volume = ''
    issue = ''
    start_page = ''
    end_page = ''
    year = ''
    publication_pos = 0
    if "." in source_title:
        publication_pos = source_title.index(".")
    elif "," in source_title:
        publication_pos = source_title.index(",")
    publication = source_title[3:publication_pos].strip()
    match = re.compile(".*\\.\\s-").search(source_title)
    if match:
        start_pos = match.end()
        match = re.compile("[\\s]*[\\d]+").match(source_title[match.end():])
        if match:
            issue = int(source_title[start_pos:][match.start():match.end()])
    match = re.compile("(?<=\\D\\d{4}\\D).+?(?=(S\\.|p?p\\.|,))").search(source_title)
    if match:
        volume = source_title[match.start():match.end()].replace(",", "").strip()
    if match:
        pages_start = match.end()
        match = re.compile("[\\s]*[0-9]+[\\s]*").match(source_title[pages_start:])
        if match:
            start_page = int(source_title[pages_start:][match.start():match.end()].strip())
            pages_start = pages_start+match.end()
            match = re.compile("-[\\s]*").match(source_title[pages_start:])
            if match:
                pages_start = pages_start+match.end()
            match = re.compile("[0-9]+[\\s]*").match(source_title[pages_start:])
            if match:
                end_page = int(source_title[pages_start:][match.start():match.end()].strip())
    match = re.compile("\\D\\d{4}(?=(\\D|,))").search(source_title)
    if match:
        year = source_title[match.start()+1:match.end()]
    item = {}
    item["publication"] = publication
    item["volume"] = volume
    item["issue"] = issue
    item["start_page"] = start_page
    item["end_page"] = end_page
    item["year"] = year
    return item

def input_with_prefill(prompt, text):
    """prefilled input, used for manual mode"""
    def hook():
        """prefilling"""
        readline.insert_text(text)
        readline.redisplay()
    readline.set_pre_input_hook(hook)
    result = input(prompt)
    readline.set_pre_input_hook()
    return result

def create_sequence(item, vals):
    """creates a sequence for anystyle training"""
    print(item)
    item = item.strip()
    indices = []
    parts = []
    option_list = ["publication", "volume", "issue", "start_page", "end_page", "publisher", "year",
                   "part-of"]
    for option in option_list:
        if vals.get(option, '') and str(vals[option]) in item:
            start = item.index(str(vals[option]))
            end = start + len(str(vals[option]))
            parts.append((option, start, end))
    parts.sort(key=lambda tup: tup[1])  # sorts in place
    prev = 0
    for part in parts:
        if part[1] != prev:
            indices.append((prev, "ignore"))
        indices.append((part[1], part[0]))
        prev = part[2]+1
    if prev != len(item):
        indices.append((prev, "ignore"))
    sequence = []
    prev = 0
    for i in range(len(indices)-1):
        text = item.strip()[prev:indices[i+1][0]].strip()
        if text:
            label = indices[i][1]
            sequence.append((label, text))
        prev = indices[i+1][0]
    if indices:
        text = item.strip()[indices[-1][0]:].strip()
        if text:
            label = indices[-1][1]
            sequence.append((label, text))
    return sequence

def write_xml(result_list, xml_file):
    """writes result as xml training data for anystyle"""
    output = xml.dom.minidom.getDOMImplementation().createDocument(None, "dataset", None)
    top_element = output.documentElement
    for item in result_list:
        sequence = output.createElement("sequence")
        for part in item:
            temp = output.createElement(part[0])
            temp.appendChild(output.createTextNode(part[1]))
            sequence.appendChild(temp)
        top_element.appendChild(sequence)
    with open(xml_file, "w+") as file:
        file.write(output.toprettyxml())

def parse_list(item_list, args):
    """parses a list of source_titles using regex and/or anystyle."""

    regex_list = []
    anystyle_list = anystyle(item_list, args)

    if not args.no_regex:
        print_verbose("extracting information with regex", args)
        for item in item_list:
            regex_list.append(parse(item))

    result_list = []

    if args.anystyle and not args.no_regex:
        result_list = combine(anystyle_list, regex_list, args)
    elif not args.no_regex:
        result_list = regex_list
    else:
        result_list = anystyle_list

    if args.interactive:
        print("Starting interactive mode. Adjust extracted values as needed.")
        result_list = interactive(item_list, result_list)

    # cleanup bad characters at the beginning/end of each entry, e.g ": text -" becomes "text"
    cleanup(result_list)
    return result_list

def map_anystyle(item):
    """map anystyle output vocabulary"""
    result = {}
    if item.get("container-title", '') and not item.get("title", ''):
        result["publication"] = item["container-title"][0].strip(BAD_CHARS)
    elif item.get("title", '') and not item.get("container-title", ''):
        result["publication"] = item["title"][0].strip(BAD_CHARS)
    elif item.get("title", '') and item.get("container-title", ''):
        result["publication"] = item["title"][0].strip(BAD_CHARS)
        result["part-of"] = item["container-title"][0].strip(BAD_CHARS)
    if item.get("volume", ''):
        result["volume"] = str(item["volume"][0]).strip(BAD_CHARS)
    if item.get("issue", ''):
        result["issue"] = str(item["issue"][0]).strip(BAD_CHARS)
    if item.get("date", ''):
        result["year"] = str(item["date"][0]).strip(BAD_CHARS)
    if item.get("pages", ''):
        for thing in item["pages"]:
            # look for weird – (not -) that anystyle uses to seperate start and end page
            match = re.compile("\\d+\\s+–\\s?\\d+").search(thing)
            if not match:
                continue
            pages = thing[match.start():match.end()]
            pages = pages.split("–")
            result["start_page"] = int(pages[0].strip(BAD_CHARS))
            result["end_page"] = int(pages[1].strip(BAD_CHARS))
    if item.get("publisher", ''):
        result["publisher"] = item["publisher"][0].strip(BAD_CHARS)
    return result

def interactive(item_list, result_list):
    """prints extracted information and allows interactive editing"""
    option_list = ["publication", "volume", "issue", "start_page", "end_page", "publisher", "year",
                   "part-of"]
    for index, item in enumerate(result_list):
        print("\n\n" + item_list[index] + "\n")
        for option in option_list:
            #print each option with prefilled extracted information
            value = input_with_prefill(option+": ", str(item.get(option, '')))
            item[option] = value
    return result_list

def anystyle(item_list, args):
    """runs anystyle"""
    anystyle_list = []
    if args.anystyle:
    # run anystyle on a tempfile
        temp_file = tempfile.NamedTemporaryFile(delete=False)
        try:
            for item in item_list:
                temp_file.write((item.strip()+"\n").encode('utf-8'))
            temp_file.close()
            cmd = 'anystyle parse '+temp_file.name
            if args.anystyle != "default":
                print_verbose("using custom anystyle model "+args.anystyle, args)
                cmd = 'anystyle -P '+args.anystyle+' parse '+temp_file.name
            print_quiet("Running anystyle. This may take some time.", args)
            anystyle_result = json.loads(subprocess.check_output(cmd, shell=True, encoding='utf-8'))
        finally:
            # remove tempfile
            os.remove(temp_file.name)
        if args.anystyle != "default":
            anystyle_list = anystyle_result
        else:
            print_verbose("extracting anystyle output", args)
            for item in anystyle_result:
                anystyle_list.append(map_anystyle(item))
    return anystyle_list

def cleanup(result_list):
    """Remove unwanted characters from results"""
    for item in result_list:
        for key, value in item.items():
            if not value:
                continue
            if not isinstance(value, str):
                try:
                    item[key] = str(value[0]).strip(BAD_CHARS)
                except TypeError:
                    item[key] = str(value).strip(BAD_CHARS)

def combine(anystyle_list, regex_list, args):
    """combines anystyle and regex information. takes the preferred one and fills gaps with
    information from the other"""
    result_list = []
    if args.prefer == "anystyle":
        print_verbose("combining output, filling gaps after anystyle with regex output", args)
        # switch the two lists if anystyle is preferred over regex.
        anystyle_list, regex_list = regex_list, anystyle_list
    else:
        print_verbose("combining output, filling gaps after regex with anystyle output", args)
    print("\nAnystyle-Length: "+str(len(anystyle_list)))
    print("Regex-Length: "+str(len(regex_list))+"\n")
    for index in range(min(len(regex_list), len(anystyle_list))):
        item = regex_list[index]
        result = {}
        for key, value in item.items():
            result[key] = value
            if not value:
                result[key] = anystyle_list[index].get(key, '')
        for key in anystyle_list[index].keys():
            if not result.get(key, ''):
                result[key] = anystyle_list[index][key]
        result_list.append(result)
    if len(anystyle_list) != len(regex_list):
        print("\n\n\nERROR: Lost "+str(abs(len(anystyle_list)-len(regex_list)))+" item(s) because regex and anystyle returned different length outputs!\nCopying Items without filling, may be incomplete.\n")
    if len(anystyle_list) < len(regex_list):
        for index in range(len(anystyle_list), len(regex_list)):
            result_list.append(regex_list[index])
    elif len(regex_list) < len(anystyle_list):
        for index in range(len(regex_list), len(anystyle_list)):
            result_list.append(anystyle_list[index])
    return result_list

