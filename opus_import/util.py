
"""two little print functions for printing with verbose and/or quiet"""

def print_verbose(message, args):
    """prints message if verbose and not quiet"""
    if not args.quiet and args.verbose:
        print(message)

def print_quiet(message, args):
    """prints message if not quiet"""
    if not args.quiet:
        print(message)
