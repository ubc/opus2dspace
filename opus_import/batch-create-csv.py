#!/usr/bin/env python3

"Batch converts .csv files into SAF directories."

import csv
import sys

import create_saf

COMBINE = False

class ArgThing:
    "replaces the normal argparser result used in create_saf.py"
    def __init__(self, ounumber, output, patents=False, filename="", no_regex=False,
                 anystyle="default", prefer="anystyle", verbose=True, quiet=False,
                 interactive=False, stats=True, training=False):
        self.ounumber = ounumber
        self.out_file = output
        self.dummy_file = filename
        self.no_regex = no_regex
        self.anystyle = anystyle
        self.prefer = prefer
        self.verbose = verbose
        self.quiet = quiet
        self.stats = stats
        self.training = training
        self.interactive = interactive
        self.patents = patents

def main(input_file, output_path):
    professorships = []
    print(input_file)
    with open(input_file, encoding="utf-8-sig") as csv_file:
        my_reader = csv.DictReader(csv_file, delimiter=";")
        for row in my_reader:
            if not "professur" in row.get("NameDE", "").lower():
                continue
            professorship = {}
            professorship["ounumber"] = row["Id"]
            professorship["name"] = row["NameDE"]
            professorship["handle"] = row["PublicationHandle"]
            professorships.append(professorship)
    for professorship in professorships:
        print("Loading "+professorship["name"])
        output_file = output_path+"/"+professorship["ounumber"]+".csv"
        args = ArgThing(professorship["ounumber"], output_file)
        items = create_saf.load_items(args)
        create_saf.write_csv(items, args)
    if not COMBINE:
        print("\nDone")
        return
    print("combining csv files.")
    keys = ["dc.title", "dc.contributor.author", "dc.contributor.affiliation", "dc.type",
            "dc.language.iso", "dc.description.abstract", "dc.subject", "dcterms.extent",
            "dc.identifier.doi", "dc.identifier.isbn", "dc.identifier.url", "dc.identifier.uri",
            "dc.contributor.advisor", "dc.date.submitted", "dc.date.issued", "dc.date.available",
            "dcterms.accessRights", "dc.publisher", "dc.relation.isbn",
            "dc.relation.doi", "dc.relation.ispartof", "dc.relation.issn",
            "dc.description.volume", "dc.description.issue", "dc.description.startpage",
            "dc.description.endpage", "dc.description.sponsorship", "payment.unit",
            "payment.payer", "payment.netAmount", "dc.description", "dc.description.intern",
            "tucbib.opus.id", "tucbib.opus.sourcetitle", "tucbib.ou.nr", "tucbib.institute.nr",
            "tucbib.publication.peerreviewed", "collections"]
    with open(output_path+"/all.csv", "w+") as out_file:
        my_writer = csv.DictWriter(out_file, fieldnames=keys)
        my_writer.writeheader()
        for professorship in professorships:
            with open(output_path+"/"+professorship["ounumber"]+".csv") as csv_file:
                my_reader = csv.DictReader(csv_file)
                for row in my_reader:
                    my_writer.writerow(row)
    print("\nDone")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: ./batch-create-csv.py professorship.csv /path/to/output")
        exit(0)
    main(sys.argv[1], sys.argv[2])
