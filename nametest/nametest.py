#!/usr/bin/env python3

"Deduplicate names in the across the given .csv files"

import csv
import os
import sys
import nameparser

class MyHumanName(nameparser.HumanName):
    "Modifies the basic HumanName class to differentiate names only by first and last name."
    def __eq__(self, other):
        if self.first.lower() == other.first.lower() and self.last.lower() == other.last.lower():
            return True
        return False

def main():
    in_path = sys.argv[1]
    out_path = sys.argv[2]


    all_names = []
    count = 0

    files = os.listdir(in_path)
    files.sort()

    for file in files:
        with open(in_path+file) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                row_names = row["dc.contributor.author"].split("||")
                count += len(row_names)
                for name in row_names:
                    if name in all_names:
                        continue
                    all_names.append(name)
    print("all names: "+str(count))
    print("variants: "+str(len(all_names)))
    print("all names loaded, comparing")

    human_names = [MyHumanName(name) for name in all_names]

    known_dups = []
    dups = {}
    indiv_names = []
    for index1, name1 in enumerate(human_names):
        if all_names[index1] in known_dups:
            continue
        duplicates = [(name1, index1)]
        for index2, name2 in enumerate(human_names[index1+1:]):
            if name1 == name2:
                duplicates.append((name2, index1+1+index2))
        longest_name = duplicates[0]
        known_dups.append(all_names[longest_name[1]])
        for name in duplicates:
            if len(all_names[name[1]]) > len(all_names[longest_name[1]]):
                longest_name = name
            known_dups.append(all_names[name[1]])
        indiv_names.append(str(longest_name[0]))
        if len(duplicates) > 1:
            duplicates = [all_names[name[1]] for name in duplicates]
            if not duplicates:
                continue
            longest_name_str = str(longest_name[0])
            for name in duplicates:
                dups[name] = longest_name_str
        else:
            dups[all_names[longest_name[1]]] = str(longest_name[0])
    print("Dedup Names: "+str(len(indiv_names)))

    for file in files:
        with open(in_path+file) as infile:
            with open(out_path+file, "w+") as outfile:
                reader = csv.DictReader(infile)
                writer = csv.DictWriter(outfile, reader.fieldnames)
                writer.writeheader()
                for row in reader:
                    row_names = row["dc.contributor.author"].split("||")
                    new_names = []
                    for name in row_names:
                        if name in known_dups:
                            new_names.append(dups[name])
                        else:
                            new_names.append(name)
                    row["dc.contributor.author"] = "||".join(new_names)
                    writer.writerow(row)
    print("Done")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: ./nametest.py /path/to/input/ /path/to/output")
        sys.exit(1)
    main()
