"""
This class handles item creation. 
It takes a header on construction and uses that header to 
determine the fields to use for future item creation. 
"""

import csv
from item import Item

class ItemFactory:
	def __init__(self, header, prof_file):
		self.header = header
		if not prof_file:
			self.prof_handles = None
			return
		self.prof_handles = {}
		with open(prof_file, encoding="utf-8-sig") as csv_file:
			my_reader = csv.DictReader(csv_file, delimiter=";")
			for row in my_reader:
				self.prof_handles[row["Id"]] = row["PublicationHandle"]

	"""
	Create a new item object.
	"""
	def newItem(self, values = None):
		item = Item(self.prof_handles)

		for index, column in enumerate(self.header):
			column = column.replace(' ', '_')
			if values == None:
				item.setAttribute(column, None)
			else:
				item.setAttribute(column, values[index])

		return item
