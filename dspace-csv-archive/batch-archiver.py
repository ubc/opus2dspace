#!/usr/bin/env python3

"Batch creation of saf directories from input csv files."

import os
import sys

from dspacearchive import DspaceArchive

def main():
    if len(sys.argv) != 4:
        print("Usage: ./batch-archiver /path/to/professorship.csv /path/to/input/ /path/to/output/")
        sys.exit(1)
    prof_file = sys.argv[1]
    input_dir = sys.argv[2]
    output_dir = sys.argv[3]

    files = []
    for index, file in enumerate(os.listdir(input_dir)):
        full_file = os.path.join(input_dir, file)
        if os.path.isfile(full_file):
            files.append((index+1, input_dir, file))

    total = len(files)

    for index, input_dir, file in files:
        print(str(index)+"/"+str(total))
        name, ext = os.path.splitext(file)
        if ext != ".csv":
            print("Unsupported file extension for "+file+", skipping")
            continue
        archive = DspaceArchive(os.path.join(input_dir, file), prof_file)
        archive.write(output_dir+name)

if __name__ == "__main__":
    main()
