# Instructions

## Run OPUS export

     python3 ./create_saf.py  -s -a "default" -d license.txt <ouid>

This will only create a .csv for the given professorship, which needs to be converted to SAF.

### Batch export

     python3 ./batch-create-csv.py professorship.csv output_dir/

`professorship.csv` has to contain a list of professorships, their ounumbers and the handles of their collections. This will create multiple .csv files in the specified output directory.

### Name deduplication

     python3 ./nametest.py input_dir/ output_dir

`input_dir/` has to contain the .csv files. The resulting, modified .csv files will be saved in the specified output directory. All data except the ow `dc.contributor.author` will be kept the same. Variations of names, with same first and last name will be recognized. The longest of these variations (e.g. with spelled-out middle name or titles) will be used instead of the other variations.

## Create DSpace Simple Package Format

     python3 ./dspace-csv-archive.py out.csv

This will create a single SAF directory from the given .csv file. The `collections` column will be ignored.

### Batch conversion

     python3 ./batch-archiver.py professorship.csv input_dir/ output_dir/

`professorship.csv` has to contain a list of professorships, their ounumbers and the handles of their collections. `input_dir/` has to contain .csv files. This will create a separate SAF directory for each .csv file in the input directory. Handles for the collections files will be taken from the `professorship.csv` file.

## All-in-one OPUS2SAF

     python3 ./create_all.py professorship.csv output_dir/

`professorship.csv` has to contain a list of professorships, their ounumbers and the handles of their collections. Loads all data from OPUS, deduplicates the names and creates SAF directories for all professorships in the professorship.csv. This will perform the 3 steps described in Batch export, Name deduplication and Batch conversion.

## Run DSpace Import

    ./dspace import --add --eperson=<person> --collection=<handle>/<id> --source=/opt/dspace/imported-data/output --mapfile=/opt/dspace/imported-data/output/mapfile

### Import into multiple collections

    ./dspace import --add --eperson=<person> --source=/opt/dspace/imported-data/output --mapfile=/opt/dspace/imported-data/mapfile

2020, Lucas Schröder, André Langer