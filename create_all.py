#!/usr/bin/env python3

import sys
import os

if len(sys.argv) != 3:
    print("Usage: ./create_all.py /path/to/professorship.csv /path/to/	tput")
    exit(1)

prof_file = sys.argv[1]
output_path = sys.argv[2]

os.system("mkdir "+output_path+"/csv")
os.system("mkdir "+output_path+"/csv_dedup")
os.system("mkdir "+output_path+"/saf")

print("Loading data")
os.system("./opus_import/batch-create-csv.py "+prof_file+" "+output_path+"/csv/")

print("Dedup names")
os.system("./nametest/nametest.py "+output_path+"/csv/ "+output_path+"/csv_dedup/")

print("Building SAF archives")
os.system("./dspace-csv-archive/batch-archiver.py "+prof_file+" "+output_path+"/csv_dedup/ "+output_path+"/saf/")

print("Done")
